import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { LoginService } from '../services/login.service';
import { TestUserService } from '../services/test-user.service';

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective {

  @Input("appHasRole") role: string = "";

  constructor(protected loginService: LoginService,
    protected templateRef: TemplateRef<any>, protected testUserService:TestUserService,protected viewContainer: ViewContainerRef) {

    this.loginService.userSubject.subscribe((user) => {
      this.updateView();
    })
  }

  ngOnInit(): void {
    this.updateView();
  }

  updateView(): void {
    this.viewContainer.clear();
    if (this.loginService.isLoggedIn() && this.testUserService.getRole() === this.role) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

}
