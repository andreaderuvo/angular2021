import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { LoginService } from '../services/login.service';

@Directive({
  selector: '[appIsLogged]'
})
export class IsLoggedDirective {

  @Input("appIsLogged") condition: boolean | undefined;

  constructor(protected loginService: LoginService,
    protected templateRef: TemplateRef<any>, protected viewContainer: ViewContainerRef) {

    this.loginService.userSubject.subscribe((user) => {
      this.updateView();
    })
  }

  ngOnInit(): void {
    this.updateView();
  }

  updateView(): void {
    console.log("UPDATEVIEW", this.condition, this.loginService.isLoggedIn());
    this.viewContainer.clear();
    if (this.loginService.isLoggedIn() == this.condition) {
      console.log(this.templateRef);
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

}
