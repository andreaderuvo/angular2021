import { AbstractControl } from '@angular/forms';

export class CustomValidators {
     static checkPwd(str: string) {
        let errorCode = "";
        if (str.length < 6) {
            errorCode = "too_short";
        } else if (str.length > 50) {
            errorCode = "too_long";
        } else if (str.search(/\d/) == -1) {
            errorCode = "no_num";
        } else if (str.search(/[a-zA-Z]/) == -1) {
            errorCode = "no_letter";
        } 
        if (!errorCode) {
            return null;
        } else {
            return {
               [errorCode] : str
            }
        }
    }

    static weakPassword(control: AbstractControl): any | null {
        return ((control: AbstractControl): { [key: string]: any } | null => {
            const regex = RegExp("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$");
            return CustomValidators.checkPwd(control.value);
        })(control);
    }
}