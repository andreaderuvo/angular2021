import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { ContactsComponent } from './components/contacts/contacts.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

//COMPONENTS
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AboutComponent } from './components/about/about.component';
import { TodosComponent } from './components/todos/todos.component';
import { TodoComponent } from './components/todo/todo.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CreateAccountComponent } from './components/create-account/create-account.component';
import { PageTitleComponent } from './components/page-title/page-title.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { ForbiddenComponent } from './components/forbidden/forbidden.component';

//DIRECTIVES
import { HasRoleDirective } from './directives/has-role.directive';
import { IsLoggedDirective } from './directives/is-logged.directive';
import { AdministrationComponent } from './components/administration/administration.component';

//PIPES
import { AlternateCasePipe } from './pipes/alternate_case.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AboutComponent,
    TodosComponent,
    TodoComponent,
    ContactsComponent,
    HomeComponent,
    NotFoundComponent,
    LoginComponent,
    CreateAccountComponent,
    PageTitleComponent,
    UnauthorizedComponent,
    ForbiddenComponent,
    HasRoleDirective,
    IsLoggedDirective,
    AdministrationComponent,
    AlternateCasePipe
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
