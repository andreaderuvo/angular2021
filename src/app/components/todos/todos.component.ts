import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/entities/todos';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos: Array<Todo>;

  constructor(private todoService: TodoService) {
    this.todos = [];

    this.todoService.read().subscribe(todos => {
      this.todos = todos;
    });
  }

  ngOnInit(): void {
  }

}
