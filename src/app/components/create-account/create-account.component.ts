import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/validators/custom_validators';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {
  createAccountForm: FormGroup;
  //https://stackoverflow.com/questions/4338267/validate-phone-number-with-javascript
  regexPhoneNumber: string = "^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$";

  isValidFormSubmitted: boolean =false;

  constructor() {
    this.createAccountForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, CustomValidators.weakPassword, Validators.minLength(8)]),
      address: new FormGroup({
        street: new FormControl(''),
        city: new FormControl(''),
        state: new FormControl(''),
        zip: new FormControl('')
      }),
      phoneNumbers: new FormArray([new FormControl('', [Validators.pattern(this.regexPhoneNumber)])])
    });
  }

  ngOnInit(): void {
  }

  ngOnSubmit(): void {
    console.log(this.createAccountForm.controls);
    if (this.createAccountForm.valid) {
      this.isValidFormSubmitted = true;
    }
  }

  get phoneNumbers() {
    return this.createAccountForm.get('phoneNumbers') as FormArray;
  }

  addPhoneNumber() {
    this.phoneNumbers.push(new FormControl('', [Validators.pattern(this.regexPhoneNumber)]));
  }
}
