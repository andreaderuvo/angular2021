import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TestUser } from 'src/app/entities/test_user';
import { LoginService } from 'src/app/services/login.service';
import { TestUserService } from 'src/app/services/test-user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private testUserService: TestUserService, private loginService: LoginService) { }

  user: TestUser = {};

  ngOnInit(): void {
    this.loginService.userSubject.subscribe((user) => {
      this.user = user;
    });
    this.user = this.testUserService.getUser() as TestUser;
  }



  logout() {
    this.loginService.logout();
    this.router.navigate(['']);
  }
}
