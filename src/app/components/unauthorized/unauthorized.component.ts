import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.css']
})
export class UnauthorizedComponent implements OnInit {

  isVisible: boolean = false;

  @Output() onAuthorizedEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  show() {
    this.onAuthorizedEvent.emit("opening");
    this.isVisible = true;
  }

  hide() {
    console.log("HIDE");
    this.isVisible = false;
    this.onAuthorizedEvent.emit("closed");
  }

}
