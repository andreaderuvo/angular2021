import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { UnauthorizedComponent } from '../unauthorized/unauthorized.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
  loginForm: FormGroup;
  showUnauthorized = false;

  @ViewChild(UnauthorizedComponent) unauthorizedComponent: any;

  constructor(private loginService: LoginService, private router: Router) {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });

    /*   this.loginForm = this.fb.group({
        "username": new FormControl("", [Validators.required, Validators.email]),
        "password": new FormControl("", [Validators.required])
      }); */
  }

  ngAfterViewInit(): void {
    console.log(this.unauthorizedComponent);
  }

  ngOnInit(): void {
  }

  onAuthorizedEvent(event: string) {
    console.log(event);
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return false;
    } else {
      if (this.loginService.login({
        email: this.loginForm.value.email,
        password: this.loginForm.value.password
      })) {
        this.router.navigate(['/todos']);
      } else {
        this.unauthorizedComponent.show();
        return false;
      }
      return true;
    }
  }
}
