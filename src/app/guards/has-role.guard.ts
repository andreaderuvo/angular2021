import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';
import { TestUserService } from '../services/test-user.service';

@Injectable({
  providedIn: 'root'
})
export class HasRoleGuard implements CanActivate {

  constructor(private router: Router, private testUserService: TestUserService, private loginService: LoginService) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let role = route.data["role"] as string;
    console.log("ROLE", role);
    if (this.loginService.isLoggedIn()) {
      if (this.testUserService.getRole() === role) {
        return true;
      }
      else {
        this.router.navigate(['403']);
        return false;
      }
    }
    this.router.navigate(['403']);
    return false;
  }

}
