import { Injectable } from '@angular/core';
import { TestUser } from '../entities/test_user';

@Injectable({
  providedIn: 'root'
})
export class TestUserService {
  private static LOCALSTORAGE_USER_KEY: string = 'user';
  testUser: TestUser | undefined;

  constructor() {
    this.testUser = JSON.parse(localStorage.getItem(TestUserService.LOCALSTORAGE_USER_KEY) as string);
  }

  getRole() {
    return this.testUser?.role;
  }

  getFullName() {
    return this.testUser?.firstName + ' ' + this.testUser?.lastName;
  }

  getUser(): TestUser {
    return this.testUser as TestUser;
  }
}
