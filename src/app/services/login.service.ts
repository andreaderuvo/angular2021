import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Login } from '../entities/login';
import { environment } from '../../environments/environment';
import { TestUser } from '../entities/test_user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private static LOCALSTORAGE_LOGGED_KEY: string = 'logged';
  private static LOCALSTORAGE_USER_KEY: string = 'user';

  userSubject = new BehaviorSubject<any>({});

  constructor() {
  }

  login(login: Login): boolean {
    if (!environment.production) {
      let testUser = Object.assign({}, environment.testUsers.find(u =>
        u.email === login.email && u.password === login.password
      ));
      if (testUser) {
        testUser.password = "";
        localStorage.setItem(LoginService.LOCALSTORAGE_LOGGED_KEY, "true");
        localStorage.setItem(LoginService.LOCALSTORAGE_USER_KEY, JSON.stringify(testUser));
        this.userSubject.next(testUser);
        return true;
      }
      this.logout();
    }
    return false;
  }

  logout() {
    localStorage.removeItem(LoginService.LOCALSTORAGE_LOGGED_KEY);
    localStorage.removeItem(LoginService.LOCALSTORAGE_USER_KEY);
    this.userSubject.next(null);
  }

  isLoggedIn() {
    return (localStorage.getItem(LoginService.LOCALSTORAGE_LOGGED_KEY)) ? true : false;
  }

  hasRole(role: string) {
    if (this.isLoggedIn()) {
      return this.user.role === role;
    } {
      return false;
    }
  }

  get user() {
    let user = localStorage.getItem(LoginService.LOCALSTORAGE_LOGGED_KEY);
    if (user) {
      return JSON.parse(user);
    }
    return null;
  }

}