import { Entity } from './entity';

export interface Login extends Entity {
    email: string;
    password: string;
}