import { Entity } from './entity';

export interface Todo extends Entity{
    userId: number,
    title: string,
    comment: string,
    completed: boolean
}