
export interface TestUser {
    email?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    role?: string;
}