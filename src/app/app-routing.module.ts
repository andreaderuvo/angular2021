import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrationComponent } from './components/administration/administration.component';
import { CreateAccountComponent } from './components/create-account/create-account.component';
import { ForbiddenComponent } from './components/forbidden/forbidden.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { TodosComponent } from './components/todos/todos.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { HasRoleGuard } from './guards/has-role.guard';
import { IsLoggedGuard } from './guards/is-logged.guard';

const routes: Routes = [{
  path: '',
  redirectTo: 'home',
  pathMatch: 'full'
}, {
  path: '',
  component: HomeComponent
}, {
  path: 'login',
  component: LoginComponent
},
{
  path: 'create/account',
  component: CreateAccountComponent
},
{
  path: 'administration',
  component: AdministrationComponent,
  canActivate: [IsLoggedGuard, HasRoleGuard],
  data: {
    role: 'ADMINISTRATOR'
  }
},
{
  path: 'todos',
  component: TodosComponent,
  canActivate: [IsLoggedGuard]
}, {
  path: '404',
  component: NotFoundComponent
}, {
  path: '403',
  component: ForbiddenComponent
}, {
  path: '401',
  component: UnauthorizedComponent
}, {
  path: '**',
  redirectTo: '/404',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
